﻿using System;

namespace LV1
{
    class Program
    {
        static void Main(string[] args)
        {
            ToDo lista = new ToDo();
            while (true)
            {
                Console.Write("Unesi zadatak: ");
                string zabiljeska = Console.ReadLine();
                if (zabiljeska == "") break;

                Console.Write("Unesi autora: ");
                string autor = Console.ReadLine();
                if (autor == "") autor = "Anonymous";

                Console.Write("Unesi vaznost: ");
                int vaznost = Convert.ToInt32(Console.ReadLine());

                Console.Write("Pracenje vremena Y/N: ");
                string vrijeme = Console.ReadLine();
                if (vrijeme == "Y" || vrijeme == "y") lista.DodajListi(new NoteTime(zabiljeska, autor, vaznost));
                else lista.DodajListi(new Note(zabiljeska, autor, vaznost));
                Console.WriteLine();
            }
            Console.WriteLine();
            lista.PrintList();
            lista.RijesiNajviseg();
            lista.PrintList();
        }
    }
}
