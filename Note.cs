﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class Note
    {
        public override string ToString()
        {
            return this.noteText + "," + this.authorName + "," + this.levelOfImportance;
        }
        private string noteText;
        private string authorName;
        private int levelOfImportance;
        public string NoteText
        {
            get { return noteText; }
            set { noteText = value; }
        }
        public string AuthorName
        {
            get { return authorName; }
            set { authorName = value; }
        }
        public int LevelOfImportance
        {
            get { return levelOfImportance; }
            set { levelOfImportance = value; }
        }
        public string getNoteText() { return this.noteText; }
        public string getAuthorName() { return this.authorName; }
        public int getLevelOfImportance() { return this.levelOfImportance; }

        public void setNoteText(string noteText) { this.noteText = noteText; }
        public void setLevelOfImportance(int levelOfImportance) { this.levelOfImportance = levelOfImportance; }
        public Note()
        {
            this.noteText = "Empty";
            this.authorName = "Name";
            this.levelOfImportance = 0;
        }
        public Note(string noteText, string authorName, int levelOfImportance)
        {
            this.noteText = noteText;
            this.authorName = authorName;
            this.levelOfImportance = levelOfImportance;
        }
        public Note(string authorName)
        {
            this.noteText = "Empty";
            this.authorName = authorName;
            this.levelOfImportance = 0;
        }
    }
}
