﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class NoteTime : Note
    {
        private DateTime time;
        public DateTime Time
        {
            get { return time; }
            set { time = value; }
        }
        public NoteTime()
        {
            time = DateTime.Now;
        }
        public NoteTime(string noteText, string authorName, int levelOfImportance, DateTime time = new DateTime()) : base(noteText, authorName, levelOfImportance)
        {
            if(time==new DateTime())
            {
                this.time = DateTime.Now;
            }
            else
            {
                this.time = time;
            }
        }
        public override string ToString()
        {
            return base.ToString() + this.time;
        }
    }
}
