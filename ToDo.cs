﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1
{
    class ToDo
    {
        private List<Note> lista;
        public ToDo()
        {
            lista = new List<Note>();
        }

        public ToDo(List<Note> list)
        {
            lista = list;
        }

        public void DodajListi(Note n)
        {
            lista.Add(n);
        }

        public void UkloniIzListe(int n)
        {
            lista.RemoveAt(n);
        }

        public Note DohvatiNote(int n)
        {
            return lista[n];
        }
        public void PrintList()
        {
            for (int i = 0; i < lista.Count; i++)
            {
                Console.WriteLine(lista[i].ToString());
            }
            Console.WriteLine();
        }
        public int NajvisaRazina()
        {
            int najveci = lista[0].LevelOfImportance;
            for (int i = 1; i < lista.Count; i++)
            {
                if (lista[i].LevelOfImportance > najveci)
                    najveci = lista[i].LevelOfImportance;
            }
            return najveci;
        }
        public void RijesiNajviseg()
        {
            int najveci = NajvisaRazina();
            for (int i = 0; i < lista.Count; i++)
            {
                if (lista[i].LevelOfImportance == najveci)
                {
                    UkloniIzListe(i);
                    i--;
                }
            }
        }
    }
}
